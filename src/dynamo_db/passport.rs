use crate::dynamo_db::{DynamoDBProvider, ENV_REGION_TITLE};
use crate::models::conditions::Is;
use crate::models::{
    login_request as lr,
    login_request::{LoginRequest, LoginRequestProvider},
    user,
    user::{User, UserProvider},
};
use crate::{BoxedError, DBResult, Provider};

use std::env;

use async_trait::async_trait;
use aws_sdk_dynamodb::Client;
use traits::db::Table;

const PROVIDER: &str = "Passport";

const ENV_CS: &str = "DB_PASSPORT_CS";
const ENV_ACCESS_KEY: &str = "DB_PASSPORT_ACCESS_KEY";
const ENV_SECRET_KEY: &str = "DB_PASSPORT_SECRET_KEY";

pub struct Passport {
    client: Client,
}

impl Provider for Passport {
    fn new() -> Self {
        Self {
            client: Self::init_client(),
        }
    }

    fn get_connection_string() -> String {
        env::var(ENV_CS).expect("No DB_PASSPORT_CS defined!")
    }
}

#[async_trait(?Send)]
impl DynamoDBProvider for Passport {
    fn get_provider<'a>() -> &'a str {
        PROVIDER
    }

    fn get_access_key() -> String {
        env::var(ENV_ACCESS_KEY).expect("No DB_PASSPORT_ACCESS_KEY defined!")
    }

    fn get_secret_key() -> String {
        env::var(ENV_SECRET_KEY).expect("No DB_PASSPORT_SECRET_KEY defined!")
    }

    fn get_region_title() -> String {
        env::var(ENV_REGION_TITLE).expect("No DB_REGION_TITLE defined!")
    }

    fn get_client(&self) -> &Client {
        &self.client
    }
}

#[async_trait(?Send)]
impl LoginRequestProvider for Passport {
    async fn get_login_request(&self, where_field: lr::Where) -> DBResult<LoginRequest> {
        let err: BoxedError = Box::new(lr::Error::NotFound);

        match where_field {
            lr::Where::Id(id) => Ok(self
                .get_from_item(LoginRequest::table(), "id", id, err)
                .await?),
            lr::Where::Value(value) => Ok(self
                .get_from_query(LoginRequest::table(), "value", value, err)
                .await?),
            _ => Err(Box::new(lr::Error::UniqueFieldsOnly)),
        }
    }

    async fn all_login_requests(&self) -> DBResult<Vec<LoginRequest>> {
        Ok(self.all(LoginRequest::table()).await?)
    }

    async fn filter_login_requests(
        &self,
        operator: Is,
        where_field: lr::Where,
        limit: Option<i32>,
    ) -> DBResult<Vec<LoginRequest>> {
        Ok(self
            .filter(LoginRequest::table(), operator, where_field, limit)
            .await?)
    }

    async fn upsert_login_request(&self, item: &LoginRequest) -> DBResult<()> {
        self.upsert(LoginRequest::table(), item).await?;

        Ok(())
    }

    async fn delete_login_request(&self, item: &LoginRequest) -> DBResult<()> {
        self.delete(LoginRequest::table(), "id", item.id().as_ref().unwrap())
            .await?;

        Ok(())
    }
}

#[async_trait(?Send)]
impl UserProvider for Passport {
    async fn get_user(&self, where_field: user::Where) -> DBResult<User> {
        let err: BoxedError = Box::new(user::Error::NotFound);

        match where_field {
            user::Where::Id(id) => Ok(self.get_from_item(User::table(), "id", id, err).await?),

            user::Where::Slug(slug) => Ok(self
                .get_from_query(User::table(), "slug", slug, err)
                .await?),

            user::Where::Email(email) => Ok(self
                .get_from_query(User::table(), "email", email, err)
                .await?),

            user::Where::Phone(phone) => Ok(self
                .get_from_query(User::table(), "phone", phone, err)
                .await?),

            _ => Err(Box::new(user::Error::UniqueFieldsOnly)),
        }
    }

    async fn all_users(&self) -> DBResult<Vec<User>> {
        Ok(self.all(User::table()).await?)
    }

    async fn filter_users(
        &self,
        operator: Is,
        where_field: user::Where,
        limit: Option<i32>,
    ) -> DBResult<Vec<User>> {
        Ok(self
            .filter(User::table(), operator, where_field, limit)
            .await?)
    }

    async fn upsert_user(&self, item: &User) -> DBResult<()> {
        self.upsert(User::table(), item).await?;

        Ok(())
    }

    async fn delete_user(&self, item: &User) -> DBResult<()> {
        self.delete(User::table(), "id", item.id().as_ref().unwrap())
            .await?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use aws_sdk_dynamodb::Credentials;

    #[test]
    fn get_credentials_works() {
        let test_ak: &str = "test_ak";
        let test_sk: &str = "test_sk";

        let creds: Credentials = Passport::get_credentials(test_ak, test_sk);

        assert_eq!(test_ak, creds.access_key_id());
        assert_eq!(test_sk, creds.secret_access_key());
    }

    #[test]
    fn get_provider_works() {
        assert_eq!(PROVIDER, Passport::get_provider());
    }

    #[test]
    fn get_endpoint_works() {
        let test_endpoint: &str = "https://test.com/";

        assert_eq!(
            test_endpoint,
            Passport::get_endpoint(test_endpoint).uri().to_string(),
        );
    }

    #[test]
    fn get_region_works() {
        let test_region: &str = "test_region";

        assert_eq!(test_region, Passport::get_region(test_region).to_string(),);
    }

    #[test]
    fn get_connection_string_works() {
        env::set_var(ENV_CS, "test_connection_string");
        assert_eq!(false, Passport::get_connection_string().is_empty());
    }

    #[test]
    fn get_access_key_works() {
        env::set_var(ENV_ACCESS_KEY, "test_access_key");
        assert_eq!(false, Passport::get_access_key().is_empty());
    }

    #[test]
    fn get_secret_key_works() {
        env::set_var(ENV_SECRET_KEY, "test_secret_key");
        assert_eq!(false, Passport::get_secret_key().is_empty());
    }

    #[test]
    fn get_region_title_works() {
        env::set_var(ENV_REGION_TITLE, "test_region_title");
        assert_eq!(false, Passport::get_region_title().is_empty());
    }

    #[test]
    #[should_panic(expected = "No DB_PASSPORT_CS defined!")]
    fn get_connection_string_should_panic() {
        env::remove_var(ENV_CS);
        Passport::get_connection_string();
    }

    #[test]
    #[should_panic(expected = "No DB_PASSPORT_ACCESS_KEY defined!")]
    fn get_access_key_should_panic() {
        env::remove_var(ENV_ACCESS_KEY);
        Passport::get_access_key();
    }

    #[test]
    #[should_panic(expected = "No DB_PASSPORT_SECRET_KEY defined!")]
    fn get_secret_key_should_panic() {
        env::remove_var(ENV_SECRET_KEY);
        Passport::get_secret_key();
    }

    #[test]
    #[should_panic(expected = "No DB_REGION_TITLE defined!")]
    fn get_region_title_should_panic() {
        env::remove_var(ENV_REGION_TITLE);
        Passport::get_region_title();
    }
}
