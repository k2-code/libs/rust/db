use crate::models::conditions::Is;
use crate::{create_model_provider_trait, DBResult};
use db_macro::{model, where_cond};
use derive_more::Display;

const MAX_ATTEMPTS: u8 = 3;
const MAX_CONFIRM_ATTEMPTS: u8 = 3;

create_model_provider_trait!(
    LoginRequestProvider,
    LoginRequest,
    get_login_request,
    all_login_requests,
    filter_login_requests,
    upsert_login_request,
    delete_login_request,
    Is,
    Where,
    DBResult
);

#[where_cond]
#[derive(Debug)]
pub enum Where {
    Id(String),
    Value(String),
    CreatedAt(String),
    UpdatedAt(String),
}

#[derive(Debug, Display)]
pub enum Error {
    #[display(fmt = "Login request not found")]
    NotFound,

    #[display(fmt = "Login request can be received only by unique fields")]
    UniqueFieldsOnly,
}

// Display implemented by derive_more
impl std::error::Error for Error {}

#[derive(Debug, Eq, PartialEq)]
pub enum AuthType {
    Email,
    Phone,
}

#[model]
#[derive(Debug)]
pub struct LoginRequest {
    value: Option<String>,
    code: Option<String>,
    attempts: Option<u8>,
    confirm_attempts: Option<u8>,
}

impl LoginRequest {
    pub fn new_value_code(value: impl Into<String>, code: impl Into<String>) -> Self {
        let mut model: Self = Self::default();

        model
            .set_value(value)
            .set_code(code)
            .set_attempts(1)
            .set_confirm_attempts(0);

        model
    }

    pub fn get_auth_type(&self) -> AuthType {
        match self.value().as_ref().unwrap().contains('@') {
            true => AuthType::Email,
            false => AuthType::Phone,
        }
    }

    pub fn is_attempts_full(&self) -> bool {
        match self.attempts() {
            Some(attempts) => attempts >= &MAX_ATTEMPTS,
            None => false,
        }
    }

    pub fn is_confirm_attempts_full(&self) -> bool {
        match self.confirm_attempts() {
            Some(confirm_attempts) => confirm_attempts >= &MAX_CONFIRM_ATTEMPTS,
            None => false,
        }
    }

    pub fn add_attempt(&mut self) -> &mut Self {
        match self.attempts() {
            Some(ca) => {
                if ca < &MAX_ATTEMPTS {
                    self.set_attempts(ca + 1);
                }
            }
            None => self.attempts = Some(1),
        };

        self.update()
    }

    pub fn add_confirm_attempt(&mut self) -> &mut Self {
        match self.confirm_attempts() {
            Some(cca) => {
                if cca < &MAX_CONFIRM_ATTEMPTS {
                    self.set_confirm_attempts(cca + 1);
                }
            }
            None => self.confirm_attempts = Some(1),
        };

        self.update()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const EMAIL: &str = "test@test.com";
    const PHONE: &str = "+11111111111";
    const CODE: &str = "12345";

    fn get_lr() -> LoginRequest {
        LoginRequest::new_value_code(PHONE, CODE)
    }

    #[test]
    fn get_auth_type_works() {
        assert_eq!(
            LoginRequest::new_value_code(EMAIL, CODE).get_auth_type(),
            AuthType::Email,
        );

        assert_eq!(
            LoginRequest::new_value_code(PHONE, CODE).get_auth_type(),
            AuthType::Phone,
        );
    }

    #[test]
    fn new_value_code_works() {
        let lr: LoginRequest = get_lr();

        assert_eq!(lr.value(), &Some(PHONE.to_string()));
        assert_eq!(lr.code(), &Some(CODE.to_string()));
        assert_eq!(lr.attempts(), &Some(1));
        assert_eq!(lr.confirm_attempts(), &Some(0));
    }

    #[test]
    fn is_attempts_not_full_works() {
        assert_eq!(false, get_lr().is_attempts_full());
    }

    #[test]
    fn is_attempts_full_works() {
        let mut lr: LoginRequest = get_lr();

        lr.set_attempts(MAX_ATTEMPTS);

        assert!(lr.is_attempts_full());
    }

    #[test]
    fn add_attempt_works() {
        let mut lr: LoginRequest = get_lr();

        lr.add_attempt();

        assert_eq!(&Some(2), lr.attempts());
    }

    #[test]
    fn is_confirm_attempts_not_full_works() {
        assert_eq!(false, get_lr().is_confirm_attempts_full());
    }

    #[test]
    fn is_confirm_attempts_full_works() {
        let mut lr: LoginRequest = get_lr();

        lr.set_confirm_attempts(MAX_CONFIRM_ATTEMPTS);

        assert!(lr.is_confirm_attempts_full());
    }

    #[test]
    fn add_confirm_attempt_works() {
        let mut lr: LoginRequest = get_lr();

        lr.add_confirm_attempt();

        assert_eq!(&Some(1), lr.confirm_attempts());
    }
}
