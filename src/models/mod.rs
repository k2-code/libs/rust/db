pub mod login_request;
pub mod user;

pub mod conditions {
    use derive_more::Display;

    #[derive(Debug, Display)]
    pub enum Is {
        #[display(fmt = "=")]
        Equals,
        #[display(fmt = "<>")]
        NotEquals,
        #[display(fmt = ">")]
        GreaterThan,
        #[display(fmt = ">=")]
        GreaterThanOrEquals,
        #[display(fmt = "<")]
        LessThan,
        #[display(fmt = "<=")]
        LessThanOrEquals,
    }
}

#[cfg(test)]
mod tests {
    use crate::models::conditions::Is;

    #[test]
    fn operator_equals_works() {
        assert_eq!("=", Is::Equals.to_string())
    }

    #[test]
    fn operator_not_equals_works() {
        assert_eq!("<>", Is::NotEquals.to_string())
    }

    #[test]
    fn operator_greater_than_works() {
        assert_eq!(">", Is::GreaterThan.to_string())
    }

    #[test]
    fn operator_greater_than_or_equals_works() {
        assert_eq!(">=", Is::GreaterThanOrEquals.to_string())
    }

    #[test]
    fn operator_less_than_works() {
        assert_eq!("<", Is::LessThan.to_string())
    }

    #[test]
    fn operator_less_than_or_equals_works() {
        assert_eq!("<=", Is::LessThanOrEquals.to_string())
    }
}
