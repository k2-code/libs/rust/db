use crate::models::conditions::Is;
use crate::{create_model_provider_trait, DBResult};
use db_macro::{model, where_cond};
use derive_more::Display;

create_model_provider_trait!(
    UserProvider,
    User,
    get_user,
    all_users,
    filter_users,
    upsert_user,
    delete_user,
    Is,
    Where,
    DBResult
);

#[where_cond]
#[derive(Debug)]
pub enum Where {
    Id(String),
    Slug(String),
    Email(String),
    Phone(String),
    FirstName(String),
    SecondName(String),
    ThirdName(String),
    AvatarUrl(String),
    CreatedAt(String),
    UpdatedAt(String),
}

#[derive(Debug, Display)]
pub enum Error {
    #[display(fmt = "User not found")]
    NotFound,

    #[display(fmt = "User can be received only by unique fields")]
    UniqueFieldsOnly,
}

// Display implemented by derive_more
impl std::error::Error for Error {}

#[model(is_active)]
#[derive(Debug)]
pub struct User {
    slug: Option<String>,
    email: Option<String>,
    phone: Option<String>,
    first_name: Option<String>,
    second_name: Option<String>,
    third_name: Option<String>,
    avatar_url: Option<String>,
}

impl User {
    pub fn new_email(email: impl Into<String>) -> Self {
        let mut model = Self::default();

        model.set_slug("").set_phone("").set_email(email);
        model
    }

    pub fn new_phone(phone: impl Into<String>) -> Self {
        let mut model = Self::default();

        model.set_slug("").set_email("").set_phone(phone);
        model
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const EMAIL: &str = "test@test.com";
    const PHONE: &str = "+11111111111";

    #[test]
    fn new_email_works() {
        assert_eq!(User::new_email(EMAIL).email(), &Some(EMAIL.to_string()));
    }

    #[test]
    fn new_phone_works() {
        assert_eq!(User::new_phone(PHONE).phone(), &Some(PHONE.to_string()));
    }
}
