pub mod meetings;
pub mod passport;
pub mod places;

use crate::models::conditions::Is;
use crate::Provider;
use crate::{BoxedError, DBResult};

use async_trait::async_trait;
use aws_sdk_dynamodb::client::fluent_builders::Scan;
use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::output::{GetItemOutput, QueryOutput};
use aws_sdk_dynamodb::{Client, Config, Credentials, Endpoint, Region};
use std::collections::HashMap;
use tokio_stream::StreamExt;
use traits::db::dynamo_db::{IntoPutItem, IntoScanExpr};

pub(crate) const ENV_REGION_TITLE: &str = "DB_REGION_TITLE";

pub type HashMapAV = HashMap<String, AttributeValue>;

#[async_trait(?Send)]
pub trait DynamoDBProvider: Provider {
    fn get_provider<'a>() -> &'a str;

    fn get_access_key() -> String;
    fn get_secret_key() -> String;
    fn get_region_title() -> String;

    fn get_credentials(
        access_key: impl Into<String>,
        secret_key: impl Into<String>,
    ) -> Credentials {
        Credentials::new(access_key, secret_key, None, None, Self::get_provider())
    }

    fn get_client(&self) -> &Client;

    fn init_client() -> Client {
        Client::from_conf(
            Config::builder()
                .credentials_provider(Self::get_credentials(
                    Self::get_access_key(),
                    Self::get_secret_key(),
                ))
                .endpoint_resolver(Self::get_endpoint(Self::get_connection_string()))
                .region(Self::get_region(Self::get_region_title()))
                .build(),
        )
    }

    fn get_region(title: impl Into<String>) -> Region {
        Region::new(title.into())
    }

    fn get_endpoint(connection_string: impl AsRef<str>) -> Endpoint {
        Endpoint::immutable(connection_string).expect("Cannot init endpoint!")
    }

    async fn get_from_item<I, T, F, V>(
        &self,
        table: T,
        field: F,
        value: V,
        not_found_err: BoxedError,
    ) -> DBResult<I>
    where
        I: From<HashMapAV>,
        T: Into<String> + std::fmt::Display,
        F: Into<String> + std::fmt::Display,
        V: Into<String> + std::fmt::Display,
    {
        let item: GetItemOutput = self
            .get_client()
            .get_item()
            .table_name(table)
            .key(field, AttributeValue::S(value.into()))
            .send()
            .await?;

        match item.item() {
            None => Err(not_found_err),
            Some(item) => Ok(I::from(item.to_owned())),
        }
    }

    async fn get_from_query<I, T, F, V>(
        &self,
        table: T,
        field: F,
        value: V,
        not_found_err: BoxedError,
    ) -> DBResult<I>
    where
        I: From<HashMapAV>,
        T: Into<String> + std::fmt::Display,
        F: Into<String> + std::fmt::Display,
        V: Into<String> + std::fmt::Display,
    {
        let item: QueryOutput = self
            .get_client()
            .query()
            .table_name(table)
            .index_name(format!("{field}_idx"))
            .key_condition_expression(format!("{field} = :{field}"))
            .expression_attribute_values(format!(":{field}"), AttributeValue::S(value.into()))
            .limit(1)
            .send()
            .await?;

        match item.count() {
            0 => Err(not_found_err),
            _ => Ok(I::from(item.items().unwrap()[0].to_owned())),
        }
    }

    async fn all<I, T>(&self, table: T) -> DBResult<Vec<I>>
    where
        I: From<HashMapAV>,
        T: Into<String>,
    {
        Ok(get_scan_items(self.get_client().scan().table_name(table)).await?)
    }

    async fn filter<I, T, WF>(
        &self,
        table: T,
        operator: Is,
        where_field: WF,
        limit: Option<i32>,
    ) -> DBResult<Vec<I>>
    where
        I: From<HashMapAV>,
        T: Into<String>,
        WF: IntoScanExpr,
    {
        let mut scan: Scan = self.get_client().scan().table_name(table);

        if let Some(limit) = limit {
            scan = scan.limit(limit);
        }

        let mut items: Vec<I> = get_scan_items(where_field.into_scan_expr(scan, operator)).await?;

        if let Some(limit) = limit {
            let l: usize = limit as usize;

            items = items.drain(..l).collect();
        }

        Ok(items)
    }

    async fn upsert<T, I>(&self, table: T, item: &I) -> DBResult<()>
    where
        T: Into<String>,
        I: IntoPutItem,
    {
        item.to_put_item(self.get_client().put_item().table_name(table))
            .send()
            .await?;

        Ok(())
    }

    async fn delete<T, F, V>(&self, table: T, field: F, value: V) -> DBResult<()>
    where
        T: Into<String>,
        F: Into<String>,
        V: Into<String>,
    {
        self.get_client()
            .delete_item()
            .table_name(table)
            .key(field, AttributeValue::S(value.into()))
            .send()
            .await?;

        Ok(())
    }
}

async fn get_scan_items<I>(scan: Scan) -> DBResult<Vec<I>>
where
    I: From<HashMapAV>,
{
    let mut items: Vec<I> = vec![];

    let result: Result<Vec<HashMapAV>, _> = scan.into_paginator().items().send().collect().await;

    for item in result? {
        items.push(I::from(item));
    }

    Ok(items)
}
