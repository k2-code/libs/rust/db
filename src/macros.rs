#[macro_export]
macro_rules! create_model_provider_trait {
    ($name:ident, $model:ty, $get:ident, $all:ident, $filter:ident, $upsert:ident, $delete:ident, $operator:ident, $where:ident, $result:ident) => {
        #[async_trait::async_trait(?Send)]
        pub trait $name {
            async fn $get(&self, field: $where) -> $result<$model>;

            async fn $all(&self) -> $result<Vec<$model>>;

            async fn $filter(
                &self,
                operator: $operator,
                where_field: $where,
                limit: Option<i32>,
            ) -> $result<Vec<$model>>;

            async fn $upsert(&self, model: &$model) -> $result<()>;
            async fn $delete(&self, model: &$model) -> $result<()>;
        }
    };
}
