pub mod models;

#[cfg(feature = "dynamo_db")]
pub mod dynamo_db;

mod macros;

pub type BoxedError = Box<dyn std::error::Error>;
pub type DBResult<T> = Result<T, BoxedError>;

pub trait Provider {
    fn new() -> Self;
    fn get_connection_string() -> String;
}

pub struct DB<DBP>
where
    DBP: Provider,
{
    dbp: DBP,
}

impl<DBP> DB<DBP>
where
    DBP: Provider,
{
    pub fn new(dbp: DBP) -> Self {
        Self { dbp }
    }
}

impl<DBP> std::ops::Deref for DB<DBP>
where
    DBP: Provider,
{
    type Target = DBP;

    fn deref(&self) -> &Self::Target {
        &self.dbp
    }
}
